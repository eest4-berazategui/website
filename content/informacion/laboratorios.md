---
title: Laboratorios
image: images/uploads/laboratorios.png
---
Los alumnos a partir de 4to año concurren a laboratorios específicos de la especialidad. Cursan allí, contenidos teóricos con una fuerte carga de prácticas, poniendo al alumno en contacto con recursos tecnológicos propios de la carrera que eligió para desempeñarse en su futuro.

## Características

La escuela cuenta con 4 laboratorios informáticos. Cada uno de estos posee 16 computadoras conectadas en red y con servicio de internet disponible en cada espacio. A cargo de cada laboratorio hay 2 docentes, los cuales dictan las clases y asisten y evalúan la tarea de los alumnos. Estos espacios de trabajo, justamente por ser laboratorios, cuentan con normativas de trabajo que se fundamentan en normas de seguridad y mantenimiento que los alumnos deberán aprender y respetar.

Los laboratorios son considerados también espacio curriculares, así considerados por la DGCyE de la provincia de Bs. As. lo que caracteriza por lo tanto a estos, como espacios evaluables y sobre los cuales cobra importancia la asistencia de los alumnos.

La actualización de los espacio de laboratorio, como así también sus requerimientos son aspectos importantes para la escuela. En el transcurso del 2014, se modificó la instalación eléctrica de 3 laboratorios, la misma estuvo a cargo de los profesores Ramirez, Ricardo y Vera, Diego, además  de la participación activa de alumnos de 7mo año.