---
title: Cómo Colaborar con el sitio
---

Este sitio web lo construimos entre todos, de manera colaborativa. 

Todo el código y el contenido del sitio se encuentran publicados en un proyecto en Gitlab.

## Reportar Errores

Si encontras algún error en la web podés reportarlo cargando un *issue* en el proyecto de Gitlab.

## Editar Contenido

Dentro de la carpeta `/content/` del proyecto está el contenido del sitio en formato Markdown.
