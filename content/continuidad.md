---
title: Continuidad Pedagógica
menu: main
---

## Seguimos Educando

Colección de materiales y recursos educativos digitales Seguimos Educando.

[www.educ.ar/recursos/150936/seguimos-educando](https://www.educ.ar/recursos/150936/seguimos-educando)

## Códigos de Classroom

En el siguiente botón bajarás una planilla de cálculo, que contiene los distintos códigos para acceder a los classroom del año y curso correspondiente. En caso de que haya materias que no poseen los códigos de accesos, deberán acceder a la tabla que se encuentra debajo y bajar las actividades de las materias que no poseen classroom. A la brevedad, dispondremos de todas las aulas virtuales de cada docente

[Tabla de Códigos por Materia](https://drive.google.com/file/d/1x4GRwsPOOixtvhWPovO_0WUe_W0HUV-J/view?usp=sharing)

## Preceptores

[Tabla de Códigos para Preceptores](https://docs.google.com/document/d/1IeF0cH4TDQ6c-Trze6x2m9bmWplONPB-ZiAwJDfC6jk/edit?usp=sharing)

---

## Tablas de Actividades

En las siguientes tablas, encontrarás las clases teórico prácticas de las materias correspondientes a tu año de estudios. Si encontrás que la carpeta está vacía, no desesperes, estamos en proceso de carga. 

### Ciclo Básico

<table class="table table-bordered table-responsive">
  <thead class="text-center">
    <tr>
      <td>
        1<sup>ro</sup>
      </td>
      <td>
        2<sup>do</sup>
      </td>
      <td>
        3<sup>ro</sup>
      </td>
      <td>
        4<sup>to</sup>
      </td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1vgJyHpvT0i1ElxvLQtAcPu_7bqUtSsoE?usp=sharing">
        Ciencias Naturales
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/17asCGzS8xRF3LU4d9MnI36_RuQoZcVVl?usp=sharing">
        Físico Química
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1NDxXoaavqxIPohID7jMlIohMa35lZtLq?usp=sharing">
        Físico Química
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1acuF1BlpvZDH4k4FCYtJ2zNkyxVNOV14?usp=sharing">
        Literatura
        </a>
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1syywKzyZu5wnv26xPgZrmJvPq55f3Y5P?usp=sharing">
        Ciencias Sociales
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1juQ42acOHtTVfuMCA-DtMeZ60UuUWokF?usp=sharing">
        Biología
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/14GL_ObHYM1FtfN06uQYIxKlBzhyd7A5d?usp=sharing">
        Biología
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1CkeCXt7oTUWKL_sdDQesV2oAGt50GtzQ?usp=sharing">
        Inglés
        </a>
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1f-hs-fuzrSggMin2LnhDZnjmt_rkHEyh?usp=sharing">
        Práctica del Lenguaje
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1CGrrEXDCRJyNUy-nJucnroAkr-HTlrft?usp=sharing">
        Prácticas de lenguaje
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1b3xJxclRlCwwitqfPhmAWvM3W04kNdwV?usp=sharing">
        Prácticas de lenguaje
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1nEHur4cZdOAz6SdA7rJlO1KpYQPXwQEZ?usp=sharing">
        Educación Física
        </a>
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1IGBMWPph9K3ifJfAoYSdCzFbttzlNwdZ?usp=sharing">
        Construcción de la Ciudadanía
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1PjRaaFR9CiMC1OY9Xvhmmh4W7vYYP-bD?usp=sharing">
        Construcción de la ciudadanía
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1nPtScngjLDlcWZY1UfDaTcE0y_hiopsq?usp=sharing">
        Construcción de la ciudadanía
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1J_sMOKe3WmRuMwqgj_ggOhcH8vTMNqpq?usp=sharing">
        Salud y Addolescencia
        </a>
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1XN6WmKG2zrRG4By2DhM6Wcf0eTLf0mdS?usp=sharing">
        Inglés
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1lr58trAC5BzARXx5TRv5tli-cUXwNAdx?usp=sharing">
        Inglés
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1tRE4JpOlX_TRTfAZucrPhLf65JXef9ks?usp=sharing">
        Inglés
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1ikVxh4kk7nIiZrKDT3HiqfSluCOOyA0I?usp=sharing">
        Historia
        </a>
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/139MOIZLUQcBF8eEl6grNyIP0I93eYfHu?usp=sharing">
        Educación Física
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1xVax35I3-Cfsa8rBJC4EnIFq_RnWKMmZ?usp=sharing">
        Educación Física
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1oLvJ5x6_GvCyA61smRlMDLuDS01r-Bx9?usp=sharing">
        Educación Física
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1aIsAJfgNlkmrmhS2CDUl974MWXtcUj83?usp=sharing">
        Geografía
        </a>
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1b0ztnPnm4M9YaPIVf7zNbH_IvcNhff7S?usp=sharing">
        Taller
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/19qu7vhxfOjysL3_BvgtLudOSKLEwot97?usp=sharing">
        Geografía
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1khFpcHSqRt39AbGJ97pxk76VXSi4Eu4_?usp=sharing">
        Geografía
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/16biPX9kpsh87nK3mpwRrJxWVwZpgvldR?usp=sharing">
        Matemática Ciclo Superior
        </a>
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1Y7fzKhUZK4OkKZqNSLSA8LCyoNwdBKHc?usp=sharing">
        Educación Artística
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1O6sYACbtMk7_ew8xiv0f2CBQS2TorZ5L?usp=sharing">
        Historia
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1PacseGx_SrNFL0XqfSE2YbspV44B6EG7?usp=sharing">
        Historia
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1Fv8K0wXM5Q4itucHJDmdaSsxPg3AYgO3?usp=sharing">
        Física
        </a>
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/17Ue53T_irz1o1bCiqeZX3VbBFgC-7Vd0?usp=sharing">
        Matemática
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/15CzTwPDHmHVLHQzsXLe7VeQr4gy4UY-U?usp=sharing">
        Taller
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1daHVv-AeOZsadFVOT5zANbtVjeuRmw06?usp=sharing">
        Taller
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/10FxG1l1z_pApP6JGRACVIb_64j8X9uDI?usp=sharing">
        Química
        </a>
      </td>
    </tr>
    <tr>
      <td>
        &nbsp;
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1qhICoiUQvMX8N2_WxDOWQrXqoFrxJwug?usp=sharing">Educación Artística
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1_Ih9TXATg1OaveotN0zdH7zkM3jQvz5o?usp=sharing">Educación Artística
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1cOyVRlrEcdWwjsvkQ8IeIf4KjiXRo9NK?usp=sharing">
        Tecnologías Electrónicas
        </a>
      </td>
    </tr>
    <tr>
      <td>
        &nbsp;
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1VRMlgZX974ySsluIgoOF96a-zPzclOAe?usp=sharing">
        Matemática
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1y8YhM7mOFrgY96bud4Yj7js6C_FeRPn5?usp=sharing">
        Matemática
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1Foykc4GPPB93VPqUEXpy91wBSQaqcj10?usp=sharing">
        Laboratorio de Programación
        </a>
      </td>
    </tr>
    <tr>
      <td>
        &nbsp;
      </td>
      <td>
        &nbsp;
      </td>
      <td>
        &nbsp;
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1X7nYKcb5KDhQ006KU3QWqL8OSFmObCNC?usp=sharing">
        Laboratorio de Hardware (Redes)
        </a>
      </td>
    </tr>
    <tr>
      <td>
        &nbsp;
      </td>
      <td>
        &nbsp;
      </td>
      <td>
        &nbsp;
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1jsrikOGdDxEI8tF0YuusjtlGCwdx4TiI?usp=sharing">
        Laboratorio de Sistemas Operativos
        </a>
      </td>
    </tr>
    <tr>
      <td>
        &nbsp;
      </td>
      <td>
        &nbsp;
      </td>
      <td>
        &nbsp;
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1xjoY5zL6_um8LQwYWSHXpXzJIXjMXKzG?usp=sharing">
        Laboratorio de Aplicaciones
        </a>
      </td>
    </tr>
  </tbody>
</table>

### Tecnicatura en Informática Profesional y Personal

<table class="table table-bordered table-responsive">
  <thead class="text-center">
    <tr>
      <td>
        5<sup>to</sup>
      </td>
      <td>
        6<sup>to</sup>
      </td>
      <td>
        7<sup>mo</sup>
      </td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1JYrqr7vKzbquH-xuoCaFLTQ0dBIKlewl?usp=sharing">
        Literatura
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1tNg05j5howTUTwJy_-evBeXJx7luewLv?usp=sharing">
        Literatura
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/10mqH3UTryDM2eCUUNgZyHfKEoFvOjn-9?usp=sharing">
        Emprendimientos Productivos y Desarrollo Local
        </a>
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1XR48NFJemumSeQc9hggTm5Ea1hY0Nn0Q?usp=sharing">
        Inglés
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1UWKHar-YqfjVHflNMWPIR03i6sek0339?usp=sharing">
        Inglés
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1xkHblwAURE1PQvtuOVom75NPyv-wF9Dp?usp=sharing">
        Evaluación de Proyectos
        </a>
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1FMxdNf7z6LOfUNegxApUa2XazR7Lddao?usp=sharing">
        Educación Física
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/13a8aZW_566aEVNlk2s5E7llvi_F4MRgW?usp=sharing">
        Educación Física
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1OyD3gzzY6lMVGY8QJ_UYgRbl1pTq3Uts?usp=sharing">
        Modelos y Sistemas
        </a>
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1c_juXQD6uZLnfRQFjRN_4DJXo3XS62fx?usp=sharing">
        Politica y ciudadania
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1lUKOFhEP-lffIXnUyuqsNuMu9Fw96OIf?usp=sharing">
        Filosofía
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1sY_kWh67kxQvB9G4FPiLFagni_uQDcys?usp=sharing">
        Base de Datos
        </a>
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1sB-1I54Ewe4NJprjvd86bGaPgCg35Uz6?usp=sharing">
        Historia
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/16RJYYyUvVspONrolp_bhyxy2EbkLyEPj?usp=sharing">
        Arte
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/10g1D6ta4Lc2uNb6jrjNnYKuZQZgcVEJ1?usp=sharing">
        Proyecto, Diseño e Implementación de Sistemas Computacionales
        </a>
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1q_XGCqah7dCYakMw74dhqrxnXSdj47do?usp=sharing">
        Geografía
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1hwuTzaxJtxL0Cvu7BISrVgE0QeC3OhdR?usp=sharing">
        Matemática Aplicada
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/14UUy4xfwwDVCQ46zwGzD8x2HQ7tnQ4sr?usp=sharing">
        Instalación, Mantenimiento y Reparación de Sistemas Computacionales
        </a>
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1lbW_nXvXb7t0nyFRjIrCxExKaI_NsDSX?usp=sharing">
        Análisis Matemático
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1Nx07M9u1B9IVqr11z73fFn5yQqCrt33g?usp=sharing">
        Sistemas Digitales
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1USaLw79p-CZupD85PBNCjQyn9UBTJFNV?usp=sharing">
        Instalación, Mantenimiento y Reparación de Redes Informáticas
        </a>
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1Eh_gihJ4YJnDd-NqSkqo_AwECfstnbo_?usp=sharing">
        Sistemas Digitales
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/11TAu8dHgK_VEicOFDuQ1MfXQaYLVh0Zn?usp=sharing">
        Investigación Operativa
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/104_y4snpIEL78gvNtaN3gSDgk5-OPCj4?usp=sharing">
        Prácticas Profesionalizantes del Sector Informática
        </a>
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1yNqMttzGzm2mo0OiCOLjqcb5M2_Cq65N?usp=sharing">
        Teleinformática
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1V3giIPzzaeuh-WBPIHzXNHrnsmJTr-gB?usp=sharing">
        Seguridad Informática
        </a>
      </td>
      <td>
        &nbsp;
      </td>
    </tr>
    <tr>
      <td>
        &nbsp;
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1ihRxoNDibfGVQIlgb46dJUiiwGX2EKZI?usp=sharing">
        Derecho del Trabajo
        </a>
      </td>
      <td>
        &nbsp;
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1kbQhaCT9dVfcGWx3zH00wMlRaQPZwCrv?usp=sharing">
        Laboratorio de Programación
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1i5USanBPetMSUBqNDjVKZojIe73mKNky?usp=sharing">
        Laboratorio de Programación
        </a>
      </td>
      <td>
        &nbsp;
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1wM3fSDC-Z5YIyoD0rbhpk2A6HaMLgHFy?usp=sharing">
        Laboratorio de Hardware
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1ai_Ya_UdIGviFxNOzXl56ZzOtaJPmmcw?usp=sharing">
        Laboratorio de Hardware (Redes)
        </a>
      </td>
      <td>
        &nbsp;
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1ClFegccMiIJmnr0orHyafmntxkslTZNq?usp=sharing">
        Laboratorio de Sistemas Operativos
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/10S0o7qQRNSLgaHbBiKZJPiDuclr73xxb?usp=sharing">
        Laboratorio de Sistemas Operativos
        </a>
      </td>
      <td>
        &nbsp;
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1XgKEx6xtSF2qIDLaqrYBn3Uudsh-uvsQ?usp=sharing">
        Laboratorio de Aplicaciones
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1yEHDas1DqDWfuj1YUm1NS98ZdAwENjz4?usp=sharing">
        Laboratorio de Aplicaciones
        </a>
      </td>
      <td>
        &nbsp;
      </td>
    </tr>
  </tbody>
</table>

### Tecnicatura en Programación

<table class="table table-bordered table-responsive">
  <thead class="text-center">
    <tr>
      <td>
        5<sup>to</sup>
      </td>
      <td>
        6<sup>to</sup>
      </td>
      <td>
        7<sup>mo</sup>
      </td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1qrS67ZgNWqKKW-H2txahlqni0Cqv2FjO?usp=sharing">
        Literatura
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1bHetgSYuUWJT-H5LLNkP8tjU2sd-GUtn?usp=sharing">
        Literatura
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1WGGgyAFixeMI6e4YW4IHrtF4TX2HWTqN?usp=sharing">
        Emprendimientos Productivos y desarrollo Local
        </a>
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/15Aj2anQpP76zivv-k4Us0xPq7aj1tNO2?usp=sharing">
        Inglés
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1FOGxwCX-GWkz3Hzmw7tODw7YJe9oa7UD?usp=sharing">
        Inglés
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/19gOvOjsXFqnCaXlEIR97__krjiuad-QT?usp=sharing">
        Evaluación de Proyectos
        </a>
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1GPwE9vhXu7JC0jNxcEBQsOZ1Bw8Y16QU?usp=sharing">
        Educación Física
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1NSXcUBUQ7PqBhf5OPBigqdTUFJ8q3nxA?usp=sharing">
        Educación Física
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/12f1bdfySVFgxAQLXf5n_P6VFq_OB6vtc?usp=sharing">
        Modelos y Sistemas
        </a>
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1bsMmqTBjxs7K_F09QXKImH_bpRVGLiBx?usp=sharing">
        Política y Ciudadanía
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1CIJT3vQ4-rzcBM-QCztZmrvPLoqWXvjA?usp=sharing">
        Filosofía
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1OhcirVVWU1It2DMnLFFu0WtgP7-dREAf?usp=sharing">
        Organización y Métodos
        </a>
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1kS2fr5MNDR4-JL95eGV7jt9le_4Rwn72?usp=sharing">
        Historia
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/16ZNVWXEY9wnc-mNUJuQDeLyJyPUieKAX?usp=sharing">
        Arte
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1wqqJHYKnyNkC56h7ArCPjbS6oe2jYZyb?usp=sharing">
        Proyecto, Diseño e Implementación de Sistemas Computacionales
        </a>
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1aak20V9x3BfGzSxyj5Rf01llns5gykHr?usp=sharing">
        Geografía
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1kIvkaKbRFJKasxasgnhohswDdk8-8byo?usp=sharing">
        Matemática Discreta
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1R_PSS1Rum4kq4XXSNgUsQ3odlBx3tVQZ?usp=sharing">
        Proyecto de Desarrollo e Implementación para Plataformas Móviles
        </a>
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1VC60nIEKhJh1duCSDxCIF6HXKQHosQgn?usp=sharing">
        Análisis Matemático
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1YdnyMQsQHDuF5-qX0lu_1hHun9qqVEAb?usp=sharing">
        Sistemas Digitales
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1LxDuegIOZDu72HV23HUgNjPttmDxMelz?usp=sharing">
        Proyecto e Implementación de Sitios Web Dinámicos
        </a>
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1lCLnAF9bHHNQU-0S4lP6MoPObk_mmvDo?usp=sharing">
        Sistemas Digitales
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1n8irIK0xETvdFeK8bor2msjEO6THPMLP?usp=sharing">
        Sistemas de Gestión y Autogestión
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1wcQJBpVvJHCPQMq0my_ni8t0Z3QocTA5?usp=sharing">
        Prácticas Profesionalizantes del Sector Informática
        </a>
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1ahhAl1AiKxLmSFeeBVGMgB_dQNZ8lDj9?usp=sharing">
        Base de Datos
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1L5Z-taWlcYlXK5XaEc0akD8M8HQ585qI?usp=sharing">
        Seguridad Informática
        </a>
      </td>
      <td>
        &nbsp;
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1XIMFP2N_A90gEyXbb79kg6Zo3R22UxmJ?usp=sharing">
        Modelos y Sistemas
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1s-hqeGnNILhe2FRJkFHx4h236PtK1ste?usp=sharing">
        Derecho del Trabajo
        </a>
      </td>
      <td>
        &nbsp;
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1v90WlVRntXwepYE4QRV12Q7puuD-M-Hj?usp=sharing">
        Laboratorio de Programación II (Estructurada)
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/19sACGk0CaceZntqHVINlDSGfySNkgBk0?usp=sharing">
        Laboratorio de Programación III
        </a>
      </td>
      <td>
        &nbsp;
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1xDHgqtFhpHekWYUFoxcMUXH3rvWa1yj2?usp=sharing">
        Laboratorio de Hardware (Redes Informáticas)
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1AX9_88oo05I0bmO28hcG9ZR_iy0b03dL?usp=sharing">
        Laboratorio de Procesos Industriales
        </a>
      </td>
      <td>
        &nbsp;
      </td>
    </tr>
    <tr>
      <td>
        <a href="https://drive.google.com/drive/folders/1AnzmQmKRzIQOPZwE74bAi1P-d8D-IgfD?usp=sharing">
        Laboratorio de Diseño Web
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/13PK2oPIA5pmgP3gVBvZFjWlFkbqUA9w_?usp=sharing">
        Desarrollo de Aplicaciones y Web Estáticas
        </a>
      </td>
      <td>
        &nbsp;
      </td>
    </tr>
    <tr class="c41">
      <td>
        <a href="https://drive.google.com/drive/folders/1wyZ3f5cuE-x98OuU41XgA_izARlOM6Ny?usp=sharing">
        Laboratorio de Diseño de Base de Datos
        </a>
      </td>
      <td>
        <a href="https://drive.google.com/drive/folders/1Y4j_KUc_aU66SFjmh-PKiwTKkeic5Odi?usp=sharing">
        Desarrollo de Aplicaciones Web Dinámicas
        </a>
      </td>
      <td>
        &nbsp;
      </td>
    </tr>
  </tbody>
</table>
