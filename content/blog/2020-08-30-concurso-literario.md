---
title: "Concurso literario de la EESTNº4 “Formación Técnica y literatura”"
date: 2020-08-30T00:00:00.000Z
image: images/uploads/concurso-literario.png
---

Primera edición del concurso literario de la EESTNº4 “Formación Técnica y literatura”

### Reglamento e Inscripción

https://sites.google.com/view/primer-concurso-literario/inicio
