---
title: "Comiendo Redes"
image: images/uploads/mic.jpg
schedule: "Martes y Jueves, 19 a 20 hs"
members: 
    - name: Profesor Vera Aranda Diego
      description: Jefe de Area
      image: "./avatar3.png"
---

Espacio de Tertulia y comentarios sobre Redes Informáticas.