---
title: "Programa Informativo"
image: images/uploads/mic.jpg
schedule: "Lunes a Viernes, 16hs"
members: 
    - name: Porfesor Nestor Pereyra
      description: Director
      image: "./avatar.png"
    - name: Carlitos, Raul y Cristina
      description: Alumnos de 5to Año
      image: "./avatar2.png"
---

Dedicado a las noticias de Cartelera Escolar
