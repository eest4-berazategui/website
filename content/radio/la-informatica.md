---
title: "La Informática"
image: images/uploads/studio.jpg
schedule: "Martes, 14 a 16 hs"
members: 
    - name: Cinthia Fernández
      description: Profesora de Informática
      image: "./avatar5.png"
---

Dedicado temas referentes a los contenidos de los diferentes laboratorios